package br.ucsal.bes20182.exercicio2;

public class Fatorial {

	public Long calcularFatorial(Long valor) {
		if(valor < 0) {
			return null;
		}
		Long resultado = 1L;
		for (int i = 1; i <= valor; i++) {
			resultado *= i;
		}
		return resultado;
	}

	public void validarNumero(Long numero) throws Exception {
		if (numero < 0 || numero > 100) {
			throw new Exception("O número é inválido (apenas números entre 0 e 100)");
		}
	}

	public void resultadoFatorial(Long valor) throws Exception {
		validarNumero(valor);
		Long resultado = calcularFatorial(valor);
		System.out.print("Fatorial = " + resultado);
	}

}
