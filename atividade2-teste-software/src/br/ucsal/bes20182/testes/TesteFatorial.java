package br.ucsal.bes20182.testes;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import br.ucsal.bes20182.exercicio2.Fatorial;

@RunWith(Parameterized.class)
public class TesteFatorial extends Assert {
	
	@Parameters(name="{index} - calcularFatorial({0})={1}")
	public static Collection<Long[]> obterCasosTeste() {
		return Arrays.asList(new Long[][] { { 0l, 1L }, { 3l, 6L }, { 5l, 120L }});
	}
	
	@Parameter
	public Long index;
	
	@Parameter(1)
	public Long esperado;


	@Test
	public void testarFatorial() {
		Fatorial fatorial = new Fatorial();
		Assert.assertEquals(fatorial.calcularFatorial(index), esperado);
	}

	@Test
	public void testeSetValor() {
		try {
			Fatorial fatorial = new Fatorial();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			System.setOut(new PrintStream(out));
			fatorial.resultadoFatorial(index);
			String console = out.toString();
			assertEquals(console, "Fatorial = " + esperado);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testeGetValor() {
		Fatorial fatorial = new Fatorial();
		String console = index + "\n8";
		ByteArrayInputStream in = new ByteArrayInputStream(console.getBytes());
		System.setIn(in);
		Long resultado = fatorial.calcularFatorial(index);

		assertEquals(resultado, esperado);
	}

}
